package app.chat.athul.viewmodels

import android.app.Application
import androidx.databinding.ObservableField
import androidx.lifecycle.*
import app.chat.athul.repository.BaseRepository
import app.chat.athul.repository.models.ChatMessage
import app.chat.athul.utils.Constants
import app.chat.athul.utils.isNullOrEmptyOrWhiteSpace
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async

class ChatDetailViewModel(val repository: BaseRepository, application: Application) : AndroidViewModel(application){

    companion object{
        const val EMPTY_MESSAGE=1
    }

    val chatMessage=ObservableField<String>()
    val chatsData=repository.chatsData
    val events=MutableLiveData<Int>()

    /**
     * Will attempt to send message to Bot Server
     *
     */
    fun onSendMessageClicked(){
        if (!chatMessage.get().isNullOrEmptyOrWhiteSpace()){
            val message=chatMessage.get()!!
            viewModelScope.async(Dispatchers.IO) {
                repository.getChatResponse(message.trim())
            }
            chatMessage.set(Constants.EMPTY_STRING)
        }else{
            events.postValue(EMPTY_MESSAGE)
        }
    }

    /**
     * Will attempt to resend the message by deleting existing one
     */
    fun onResendClicked(item: ChatMessage?) {
        viewModelScope.async(Dispatchers.IO) {
            item?.let {
                val message=it.message
                repository.deletePendingMessage(it.id)
                repository.getChatResponse(message)
            }
        }
    }
}



class ChatDetailViewModelFactory( val repository: BaseRepository,val application: Application) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ChatDetailViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return ChatDetailViewModel(repository,application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}