package app.chat.athul.repository.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import app.chat.athul.repository.models.ChatMessage

@Dao
interface ChatDao {

    @Query("SELECT * FROM chats ORDER BY id ASC")
    fun getAllChats(): LiveData<List<ChatMessage>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(chatMessage: ChatMessage):Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(chatMessages: List<ChatMessage>):List<Long>

    @Query("DELETE FROM chats")
    suspend fun deleteAll()

    @Query("UPDATE chats SET status = :status WHERE id = :messageId")
    suspend fun setMessageStatus(messageId: Long, status: String)

    @Query("DELETE FROM chats WHERE id=:id")
    suspend fun deleteById(id: Int)
}