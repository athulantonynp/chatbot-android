package app.chat.athul.repository

import app.chat.athul.repository.api.ApiManager
import app.chat.athul.repository.db.ChatBase
import app.chat.athul.repository.models.ChatMessage
import app.chat.athul.utils.Constants

class BaseRepository(val chatBase: ChatBase) {

    val chatsData=chatBase.chatDao().getAllChats()

    suspend fun getChatResponse(message:String){
        var questionId=-1L
        try {

            questionId= onChatMessageRecorded(ChatMessage(Constants.EMPTY_STRING,Constants.EMPTY_STRING
                ,message,Constants.EMPTY_STRING),false,questionId)
            val response=ApiManager.getInstance().getChat(message)
            if (response.success==1){
                onChatMessageRecorded(response.message.also { it.status=ChatMessage.STATUS_SUCESS },true,questionId)
            }


        } catch (e:Exception){
            //catching common exception and treating it as a failed attempt.Needs revision
           setMessageStatus(ChatMessage.STATUS_FAILED,questionId)
        }
    }

    suspend fun onChatMessageRecorded(message: ChatMessage,isBot:Boolean,questionMessageId:Long):Long {
        try {
            message.isBot=isBot
            val result=chatBase.chatDao().insert(message)
            setMessageStatus(ChatMessage.STATUS_SUCESS,questionMessageId)
            return result
        }catch (e:Exception){
            e.printStackTrace()
        }
        return -1L
    }

    suspend fun setMessageStatus(status:String,messageId:Long){
        if (messageId>=0){
            chatBase.chatDao().setMessageStatus(messageId,status)
        }
    }

    suspend fun deletePendingMessage(id:Int){
        chatBase.chatDao().deleteById(id)
    }
}