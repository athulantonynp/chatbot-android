package app.chat.athul.repository.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

/**
 * data class for chat message
 */

@Entity(tableName = "chats")
data class ChatMessage(

    @SerializedName("chatBotName")
    @ColumnInfo(name = "chatBotName")
    val chatBotName:String,

    @SerializedName("chatBotID")
    @ColumnInfo(name = "chatBotID")
    val chatBotID:String,

    @SerializedName("message")
    @ColumnInfo(name = "message")
    val message:String,

    @SerializedName("emotion")
    @ColumnInfo(name = "emotion")
    val emotion:String?=null,

    @ColumnInfo(name = "is_bot")
    var isBot:Boolean=true
){
    @PrimaryKey(autoGenerate = true)
    var id:Int=0

    @ColumnInfo(name = "status")
    var status:String?=null

    companion object{
        val STATUS_SUCESS="SUCCESS"
        val STATUS_FAILED="FAILED"
    }

    override fun equals(other: Any?): Boolean {
        if (other is ChatMessage){
            return  other.message==this.message && other.id==this.id
        }
        return false
    }
}
