package app.chat.athul.repository.api

import app.chat.athul.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class ApiManager {

    companion object{

        @Volatile
        private var INSTANCE: ApiService? = null

        //Returns singleton instance of ApiService
        fun getInstance(): ApiService {
            val interceptor = HttpLoggingInterceptor()

            interceptor.level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE

            val client = OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build()

            return INSTANCE ?: synchronized(this) {
                val retrofit = Retrofit.Builder()
                    .baseUrl(BuildConfig.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build()

                val service = retrofit.create(ApiService::class.java)

                INSTANCE = service
                service
            }
        }
    }
}