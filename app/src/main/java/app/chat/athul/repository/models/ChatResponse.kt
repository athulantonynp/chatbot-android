package app.chat.athul.repository.models

import com.google.gson.annotations.SerializedName


/*
Model class for chatbot response from server*/

data class ChatResponse (
    @SerializedName("success")
    val  success:Int,

    @SerializedName("errorMessage")
    val errorMessage:String?=null,

    @SerializedName("message")
    val message:ChatMessage

)