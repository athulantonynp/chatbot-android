package app.chat.athul.repository.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import app.chat.athul.repository.models.ChatMessage

@Database(entities = arrayOf(ChatMessage::class), version = 1, exportSchema = false)
public  abstract  class ChatBase :RoomDatabase(){

    abstract fun chatDao(): ChatDao

    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: ChatBase? = null

        fun getInstance(context: Context): ChatBase {
            // if the INSTANCE is not null, then return it,
            // if it is, then create the database
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    ChatBase::class.java,
                    "chat_db"
                ).build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }
}