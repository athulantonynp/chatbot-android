package app.chat.athul.repository.api

import app.chat.athul.BuildConfig
import app.chat.athul.repository.models.ChatResponse
import retrofit2.http.GET
import retrofit2.http.Query

/*
Defines all app related API endpoints
 */
interface ApiService {

    @GET("chat")
    suspend fun getChat(@Query("message") message:String,
                @Query("apiKey") apiKey:String=BuildConfig.API_KEY,
                @Query("chatBotID")botID:String=BuildConfig.CHATBOT_ID,
                @Query("externalID")extID:String=BuildConfig.EXTERNAL_ID
    ):ChatResponse


}

