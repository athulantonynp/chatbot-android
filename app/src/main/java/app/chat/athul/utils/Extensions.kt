package app.chat.athul.utils

import android.content.Context
import android.widget.Toast

fun String?.isNullOrEmptyOrWhiteSpace():Boolean{
    var onlySpace=true
    this?.let {
        onlySpace= it.trim().isEmpty()
    }

    return onlySpace
}

fun Context.showErrorToast(string:Int) =
    Toast.makeText(this, this.getString(string), Toast.LENGTH_SHORT).show()