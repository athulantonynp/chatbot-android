package app.chat.athul.views.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import app.chat.athul.R
import app.chat.athul.repository.models.ChatMessage
import app.chat.athul.viewmodels.ChatDetailViewModel

class ChatsAdapter(val viewModel: ChatDetailViewModel) : ListAdapter<ChatMessage,ChatsAdapter.ViewHolder>(ChatListDiff()) {

    companion object{
        val CHAT_TYPE_BOT=1
        val CHAT_TYPE_USER=2
    }

    open class ViewHolder( view: View):RecyclerView.ViewHolder(view)

    class BotChatViewHolder(val view:View):ViewHolder(view){
        val tvText=  view.findViewById<TextView>(R.id.tv_chat_message)

        fun bind(item: ChatMessage?, position: Int) {
            tvText.text=item?.message
        }

    }

    class UserChatViewHolder(val view: View,val viewModel: ChatDetailViewModel):ViewHolder(view){
        val ivResend=view.findViewById<ImageView>(R.id.iv_resend)
        val tvText=  view.findViewById<TextView>(R.id.tv_chat_message)

        fun bind(item: ChatMessage?, position: Int) {

            tvText.text=item?.message
            ivResend.setOnClickListener { viewModel.onResendClicked(item) }

            if (item?.status==ChatMessage.STATUS_FAILED){
                ivResend.visibility=View.VISIBLE
            }else{
                ivResend.visibility=View.GONE
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        if (viewType== CHAT_TYPE_BOT){
            val view: View = LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item_chat_item_bot, parent, false)
            return BotChatViewHolder(view)
        }else{
            val view: View = LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item_chat_item_user, parent, false)
            return UserChatViewHolder(view,viewModel)
        }

    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (holder is BotChatViewHolder){
            (holder).bind(getItem(position),position)
        }

        if (holder is UserChatViewHolder){
            holder.bind(getItem(position),position)
        }

    }

    override fun getItemViewType(position: Int): Int {
        val data=getItem(position)
        return if (data.isBot){ CHAT_TYPE_BOT }else{ CHAT_TYPE_USER }
    }

    override fun getItemId(position: Int)=getItem(position).id.toLong()

}


class ChatListDiff: DiffUtil.ItemCallback<ChatMessage>(){
    override fun areItemsTheSame(oldItem: ChatMessage, newItem: ChatMessage): Boolean {
        return (oldItem.id==newItem.id)
    }

    override fun areContentsTheSame(oldItem: ChatMessage, newItem: ChatMessage): Boolean {
        return (oldItem.message==newItem.message)
    }

}