package app.chat.athul.views.activities

import android.os.Bundle
import android.view.Menu
import androidx.activity.viewModels
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.chat.athul.R
import app.chat.athul.databinding.ActivityChatDetailsBinding
import app.chat.athul.repository.models.ChatMessage
import app.chat.athul.utils.showErrorToast
import app.chat.athul.viewmodels.ChatDetailViewModel
import app.chat.athul.viewmodels.ChatDetailViewModelFactory
import app.chat.athul.views.App
import app.chat.athul.views.adapters.ChatsAdapter


class ChatDetailsActivity : AppCompatActivity() {


    private val viewModel: ChatDetailViewModel by viewModels {
        ChatDetailViewModelFactory(
            (application as App).repository,
            application
        )
    }

    private val binding: ActivityChatDetailsBinding by lazy {
        ActivityChatDetailsBinding.inflate(
            layoutInflater
        )
    }
    private val adapter: ChatsAdapter by lazy { ChatsAdapter(viewModel) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        init()
        initViews()
        initObservers()
    }

    private fun init() {
        setContentView(binding.root)
        binding.viewModel = viewModel
    }

    private fun initViews() {
        val toolbar: Toolbar = findViewById(R.id.tb_chat)
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
            this,
            binding.drawerLayout,
            binding.tbChat,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        binding.drawerLayout.addDrawerListener(toggle)
        toggle.isDrawerIndicatorEnabled = true
        toggle.syncState()

        binding.navView.setNavigationItemSelectedListener {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
            return@setNavigationItemSelectedListener true
        }

        binding.rvChats.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        adapter.setHasStableIds(true)
        binding.rvChats.adapter = adapter


    }

    private fun initObservers() {

        viewModel.chatsData.observe(this@ChatDetailsActivity, Observer { onDataRecieved(it) })
        viewModel.events.observe(this@ChatDetailsActivity, Observer { handleEvents(it) })
    }

    private fun onDataRecieved(it: List<ChatMessage>?) {
        adapter.submitList(it)
        scrollToLastPosition()
    }

    /*
    Recyclerview may not be ready to accept the action.
    So, adding it to a delayed post()
     */
    private fun scrollToLastPosition() {
        try {
            binding.rvChats.postDelayed({
                binding.rvChats.layoutManager?.scrollToPosition(adapter.currentList.size - 1)
            }, 100)
        } catch (e: Exception) {

        }
    }


    private fun handleEvents(it: Int) {
        when (it) {
            ChatDetailViewModel.EMPTY_MESSAGE -> {
                showErrorToast(R.string.empty_message)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.drawer, menu)
        return true
    }

}