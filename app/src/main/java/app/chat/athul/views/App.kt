package app.chat.athul.views

import android.app.Application
import app.chat.athul.repository.BaseRepository
import app.chat.athul.repository.db.ChatBase

class App : Application() {

    lateinit var repository: BaseRepository

    override fun onCreate() {
        super.onCreate()
        initVariables()
    }

    /**
     * Initializes variables on application starts
     * This should be injected by a DI framework later
     * Proceeding with manual invocation for the time being
     */

    private fun initVariables(){
        repository=BaseRepository(ChatBase.getInstance(this))
    }
}