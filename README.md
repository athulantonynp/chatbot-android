
# ChatBot Android

## How to build app from the sources
  The following information concentrates on compiling on **LINUX** machines.
### Prerequisites
-   JDK 8
-   Latest Android Studio
-   Latest Android platform tools
-   Android SDK 21 or newer
-   Git for Linux

### Build Entri
Assuming your working directory is *work*

     $ cd work
Clone App


     $ git clone https://athulantonynp@bitbucket.org/athulantonynp/chatbot-android.git
Navigate to project directory

     $ cd chatbot


#### Troubleshooting

If you face any issue make sure the following checks are ticked
1) Having proper JDK path in your environment
2) Code has no error

To make sure your code is not causing the issue, run a gradle tasks from android studio and see if it succeeds or not
For running gradle task
1) Open Android studio
2) Click on Gradle tab on the studio
3) Tasks -> build -> your task

